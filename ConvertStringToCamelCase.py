def to_camel_case(text):
    if len(text) == 0:
        return ''

    # Split the text by either dash or underscore
    words = text.replace('-', ' ').replace('_', ' ').split()

    # Initialize the result with the first word
    result = [words[0]]

    # Capitalize subsequent words and append to result
    for word in words[1:]:
        result.append(word.capitalize())

    # Join the words in result to form the final camel case string
    return ''.join(result)

# Example usage:
print(to_camel_case('the-quick_brown-fox'))  # theQuickBrownFox
print(to_camel_case('The-quick_brown-fox'))  # TheQuickBrownFox
print(to_camel_case('this_is_a_test'))       # thisIsATest
print(to_camel_case('This_is_a_test'))       # ThisIsATest
