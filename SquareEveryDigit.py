def square_digits(num):

    convert_to_string = str(num)
    result = ""



    for i in range(len(convert_to_string)):
        squared_digit = int(convert_to_string[i]) ** 2
        result += str(squared_digit)

    print(int(result))
    return int(result)

print(square_digits(12345))
