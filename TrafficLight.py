def update_light(current):
    # Your code here.

    if current is "green":
        return "yellow"
    elif current is "yellow":
        return "red"
    elif current is "red":
        return "green"
    else:
        return None
        