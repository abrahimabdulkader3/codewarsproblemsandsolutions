def reverse_letter(st):

    result = ""
    for char in st[::-1]:
        if char.isalpha():
            result += char

    return result
