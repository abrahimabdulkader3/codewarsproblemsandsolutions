def solve(s):

    countUppercase = 0
    countLowercase = 0

    for i in range(len(s)):
        if s[i].islower():
            countLowercase += 1
        elif s[i].isupper():
            countUppercase += 1

    if countLowercase == countUppercase:
        return s.lower()

    elif countLowercase > countUppercase:
        return s.lower()

    elif countUppercase > countLowercase:
        return s.upper()
