def count(s):
    # The function code should be here

    if len(s) == 0:
        return {}

    dict = {}
    for char in s:
        dict[char] = dict.get(char, 0) + 1

    return dict

        